<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('index');
});
Route::get('/form_customer', function () {
    return view('form_customer');
});
Route::get('/form_animal', function () {
    return view('form_animal');
});
Route::get('/form_doctor', function () {
    return view('form_doctor');
});
Route::get('/datalist', function () {
    return view('datalist');
});
Route::get('/information', function () {
    return view('information');
});
Route::get('/succ_incustomer', function () {
    return view('succ_incustomer');
});
Route::get('/view_editcustomer', function () {
    return view('view_editcustomer');
});
Route::get('/succ_inanimal', function () {
    return view('succ_inanimal');
});
Route::get('/datalist_animal', function () {
    return view('datalist_animal');
});
Route::get('/treatment', function () {
    return view('treatment');
});
Route::get('/Contactus', function () {
    return view('Contactus');
});
Route::get('/product', function () {
    return view('product');
});
Route::get('/dognews', function () {
    return view('dognews');
});
Route::get('/catnews', function () {
    return view('catnews');
});
Route::get('/rabbitnews', function () {
    return view('rabbitnews');
});
Route::get('/hamsternews', function () {
    return view('hamsternews');
});
Route::get('/spa', function () {
    return view('spa');
});
Route::get('/shower', function () {
    return view('shower');
});
Route::get('/hotelcat', function () {
    return view('hotelcat');
});
Route::get('/hoteldog', function () {
    return view('hoteldog');
});
Route::get('/hotelother', function () {
    return view('hotelother');
});
Route::get('/succ_contact', function () {
    return view('succ_contact');
});



Route::post('/insertCostomer', 'Controller@insertCostomer');
Route::post('/insertAnimal', 'Controller@insertAnimal');

Route::get('/datalist', 'Controller@selDatalist');
Route::get('/view_editcustomer', 'Controller@view_edit');
Route::get('/form_animal', 'Controller@addAnimal');
Route::get('/form_customer', 'Controller@selProvince');
Route::get('/datalist_animal', 'Controller@selDatalistAnimal');
Route::get('/shower', 'Controller@selAnimalType');
Route::get('ajaxSendAnimal/{typeAnimal_id}', 'Controller@AjaxTypeAnimal');

Route::post('/insertShower', 'Controller@insertShower');





<?php

namespace App\Http\Controllers;


use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function insertCostomer(Request $request)
    {

        $cusGen = DB::table('customers')->orderBy('cus_ID', 'desc')->first();
        $c = $cusGen->cus_ID;
        $Customergen = $c . date("dmys");

        $cus_name = $request['cus_name'];
        $cus_address = $request['cus_address'];
        $cus_mail = $request['cus_mail'];
        $cus_phone = $request['cus_phone'];
        $province_ID = $request['province_ID'];
        $amphoe_ID = $request['amphoe_ID'];
        $district_ID = $request['district_ID'];
        $create_date = $request['create_date'];
        $create_date2 = $request['create_date2'];
        $zipcode_ID = $request['zipcode_ID'];
        $cus_remark = $request['cus_remark'];
        $cus_no = $Customergen;


        $dataCus = array('cus_name' => $cus_name,
            'cus_address' => $cus_address,
            'cus_mail' => $cus_mail,
            'cus_phone' => $cus_phone,
            'province_ID' => $province_ID,
            'amphoe_ID' => $amphoe_ID,
            'district_ID' => $district_ID,
            'create_date' => $create_date,
            'create_date2' => $create_date2,
            'zipcode_ID' => $zipcode_ID,
            'cus_remark' => $cus_remark,
            'cus_no' => $cus_no);

        DB::table('customers')->insert($dataCus);
        return redirect('succ_incustomer');
    }

    function selProAmDis()
    {

    }

    function selDatalist()
    {
        $selCus = DB::table('customers')->get();
        return view('datalist', ['selCus' => $selCus]);
    }

    function insertAnimal(Request $request)
    {
        $animalgen = DB::table('animal')->orderBy('ani_ID', 'desc')->first();
        $a = $animalgen->ani_ID;
        $anigen = $a . date("dmys");

        $ani_no = $request['typeAnimal'] . $anigen;
        $ani_name = $request['ani_name'];
        $ani_sick = $request['ani_sick'];
        $ani_remark = $request['ani_remark'];
        $ani_createdate = $request['ani_createdate'];
        $ani_createdate2 = $request['ani_createdate2'];
        $cus_no = $request['cus_no'];
        $ani_outher = $request['ani_outher'];
        $ani_breed = $request['ani_breed'];

        $data = array('ani_no' => $ani_no,
            'ani_name' => $ani_name,
            'ani_sick' => $ani_sick,
            'ani_remark' => $ani_remark,
            'ani_createdate' => $ani_createdate,
            'ani_createdate2' => $ani_createdate2,
            'cus_no' => $cus_no,
            'ani_outher' => $ani_outher,
            'ani_breed' => $ani_breed,);

        DB::table('animal')->insert($data);
        return redirect('succ_inanimal');
    }

    function view_edit(Request $request)
    {
        if (isset($request['viewCustomer'])) {
            $view = DB::table('customers')->where('cus_ID', $request['viewCustomer'])->get();

            $pro = DB::table('province')->get();

            return view('view_editcustomer'
                , ['view' => $view], ['pro' => $pro]);
        }
    }

    function addAnimal(Request $request)
    {
        if (isset($request['addAnimal'])) {
            $add = DB::table('customers')->where('cus_id', $request['addAnimal'])->get();
            return view('form_animal', ['add' => $add]);
        }
    }


    function selProvince()
    {
        $pro = DB::table('province')->get();
        return view('form_customer', ['pro' => $pro]);

    }


    function AjaxSendP_A_D_Z($id)
    {

        $province_id = $id;
        $pro = DB::table('amphur')->where('PROVINCE_ID', $province_id)->get();

        foreach ($pro as $p) {
            echo '<option value="' . $p->AMPHUR_ID . '">' . $p->AMPHUR_NAME . '</option>';
        }
        $area_id = $id;
        $area = DB::table('district')->where('AMPHUR_ID', $area_id)->get();

        foreach ($area as $a) {
            echo '<option value="' . $a->DISTRICT_ID . '">' . $a->DISTRICT_NAME . '</option>';
        }
        $des_id = $id;
        $des = DB::table('zipcode')->where('DISTRICT_ID', $des_id)->get();

        foreach ($des as $d) {
            echo '<option value="' . $d->ZIPCODE_ID . '">' . $d->ZIPCODE . '</option>';
        }
    }

    function selDatalistAnimal()
    {
        $selAni = DB::table('animal')->get();
        return view('datalist_animal', ['selAni' => $selAni]);
    }

    function selAnimalType()
    {
        $selA = DB::table('animal_type')->get();
        return view('shower', ['selA' => $selA]);
    }

    function AjaxTypeAnimal($id)
    {
        $typeAnimal_id = $id;
        $count = DB::table('service')->where('type_ID', $typeAnimal_id)->get();
        echo '<option value="" disabled selected>บริการ</option>';
        foreach ($count as $c) {
            echo '<option value="' . $c->ser_ID . '">' . $c->ser_name . '</option>';

        }

    }

    function insertContact(Request $request)
    {
        $c1 = $request['con_n'];
        $c2 = $request['con_l'];

        $con_name = $c1 . ' ' . $c2;
        $con_tel = $request['con_tel'];
        $con_email = $request['con_email'];
        $con_address = $request['con_address'];
        $con_detail = $request['con_detail'];
        $con_date = $request['con_date'];


        $dataCon = array('con_name' => $con_name,
            'con_tel' => $con_tel,
            'con_email' => $con_email,
            'con_address' => $con_address,
            'con_detail' => $con_detail,
            'con_date' => $con_date,);


        DB::table('contact')->insert($dataCon);
        return redirect('succ_contact');
    }

    function insertShower(Request $request)
    {
        $c1 = $request['cus_n'];
        $c2 = $request['cus_l'];

        $cus_name = $c1 . ' ' . $c2;
        $cus_phone = $request['cus_phone'];
        $cus_mail = $request['cus_mail'];
        $type_ID = $request['type_ID'];
        $ser_ID = $request['ser_ID'];
        $sho_price = $request['sho_price'];
        $extra_price = $request['extra_price'];
        $cus_date = $request['cus_date'];
        $total = $request['total'];


        $dataCon = array('cus_name' => $cus_name,
            'cus_phone' => $cus_phone,
            'cus_mail' => $cus_mail,
            'type_ID' => $type_ID,
            'ser_ID' => $ser_ID,
            'sho_price' => $sho_price,
            'extra_price' => $extra_price,
            'cus_date' => $cus_date,
            'total' => $total,);


        DB::table('service_shower')->insert($dataCon);
        return redirect('succ_contact');
    }

}

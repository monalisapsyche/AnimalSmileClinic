@extends('layout')
@section('content')

    <br><br>
    <div class="row">
        <div class="col-xs-12" color="#0000FF">
            <div style="margin-left:7%;margin-right:0%">
                <h2 class="w3-text-blue" style="text-shadow:1px 1px 0 #444">
                    <span class="w3-tag w3-jumbo w3-purple">H</span>
                    <span class="w3-tag w3-jumbo w3-black">O</span>
                    <span class="w3-tag w3-jumbo w3-purple">T</span>
                    <span class="w3-tag w3-jumbo w3-black">E</span>
                    <span class="w3-tag w3-jumbo w3-purple">L</span>
                    <br><br>
                    <p class="w3-text-purple"><b>โรงแรมเพื่อสัตว์เลี้ยงที่คุณรัก</b></p></h2>
                </span>
            </div>
        </div>
    </div>
    <br> <br>
    <div style="margin-left: 7%;margin-right: 7%">
        <div class="row">

            <div class="col-md-3">
                <div class="w3-card-4">
                    <img src="{{URL::to('img\hotel\d1.jpg')}}" class="img-responsive" style="max-width: 103%">
                </div>
            </div>
            <div class="col-md-3">
                <br>
                <ul>
                    <p class="w3-text-purple" style="font-size: 20px;"><b>﻿﻿﻿China Room</b></p>
                    <p><b>฿450.00</b></p>
                    <li>
                        <p style="font-size: 18px;">
                            อยู่ในห้องส่วนตัวสุนัข(แยกห้องละตัว) ไม่ขังกรงค่ะ
                        </p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">
                            สามารถนำน้องหมามาพักห้องเดียวกันได้<br>
                            (ในกรณีฝากมากกว่า 1 ตัวราคาคิดเป็นต่อตัว)
                        </p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">
                            มีสนามหญ้าให้วิ่งเล่นออกกำลังกาย
                        </p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">
                            อาหาร 2 มื้อ วิ่งเล่นวันละ 3 ครั้ง
                        </p>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="w3-card-4">
                    <img src="{{URL::to('img\hotel\d4.jpg')}}"
                         class="img-responsive" style="max-width:100%">
                </div>
            </div>
            <div class="col-md-3">
                <br>
                <ul>
                    <p class="w3-text-purple" style="font-size: 20px;">﻿﻿﻿<b>Japan Room</b></p>
                    <p><b>฿650.00</b></p>
                    <li>
                        <p style="font-size: 18px;">
                            อยู่ในห้องส่วนตัวสุนัข(แยกห้องละตัว) ไม่ขังกรงค่ะ
                        </p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">
                            สามารถนำน้องหมามาพักห้องเดียวกันได้<br>
                            (ในกรณีฝากมากกว่า 1 ตัวราคาคิดเป็นต่อตัว)
                        </p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">
                            มีสนามหญ้าให้วิ่งเล่นออกกำลังกาย
                        </p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">
                            อาหาร 2 มื้อ วิ่งเล่นวันละ 3 ครั้ง
                        </p>
                    </li>
                </ul>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-3">
                <div class="w3-card-4">
                    <img src="{{URL::to('img\hotel\d2.jpg')}}"
                         class="img-responsive" style="max-width: 106%">
                </div>
            </div>
            <div class="col-md-3">
                <br>
                <ul>
                    <p class="w3-text-purple" style="font-size: 20px;"><b>England Room</b></p>
                    <p><b>฿350.00</b></p>
                    <li>
                        <p style="font-size: 18px;">
                            อยู่ในห้องส่วนตัวสุนัข(แยกห้องละตัว) ไม่ขังกรงค่ะ
                        </p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">
                            สามารถนำน้องหมามาพักห้องเดียวกันได้<br>
                            (ในกรณีฝากมากกว่า 1 ตัวราคาคิดเป็นต่อตัว)
                        </p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">
                            มีสนามหญ้าให้วิ่งเล่นออกกำลังกาย
                        </p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">
                            อาหาร 2 มื้อ วิ่งเล่นวันละ 3 ครั้ง
                        </p>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="w3-card-4">
                    <img src="{{URL::to('img\hotel\d4.jpg')}}" class="img-responsive" style="max-width: 100%">
                </div>
            </div>
            <div class="col-md-3">
                <br>
                <ul>
                    <p class="w3-text-purple" style="font-size: 20px;">﻿﻿﻿<b>France Room</b></p>
                    <p><b>฿650.00</b></p>
                    <li>
                        <p style="font-size: 18px;">
                            อยู่ในห้องส่วนตัวสุนัข(แยกห้องละตัว) ไม่ขังกรงค่ะ
                        </p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">
                            สามารถนำน้องหมามาพักห้องเดียวกันได้<br>
                            (ในกรณีฝากมากกว่า 1 ตัวราคาคิดเป็นต่อตัว)
                        </p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">
                            มีสนามหญ้าให้วิ่งเล่นออกกำลังกาย
                        </p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">
                            อาหาร 2 มื้อ วิ่งเล่นวันละ 3 ครั้ง
                        </p>
                    </li>
                </ul>
            </div>
        </div>
        <hr>
    </div>
    <div class="w3-container">
        <div class="w3-panel w3-pale-red w3-leftbar w3-border-red">
            <h4><p><b>สิ่งจำเป็นที่เจ้าของสุนัขควรเตรียมมา</b></p></h4>
            <p>-  สายจูง หรือชุดรัดอกสายจูง</p>
            <p>-  เบาะ ตุ๊กตา ของเล่นที่สุนัขคุ้นเคย</p>
        </div>
    </div>

    <script>
        /*------------------------navbar------------------------------------------------*/
        $(document).ready(function () {

            //Calculate the height of <header>
            //Use outerHeight() instead of height() if have padding
            var aboveHeight = $('header').outerHeight();

            //when scroll
            $(window).scroll(function () {

                //if scrolled down more than the header’s height
                if ($(window).scrollTop() > aboveHeight) {

                    // if yes, add “fixed” class to the <nav>
                    // add padding top to the #content
                    //  (value is same as the height of the nav)
                    $('nav').addClass('fixed').css('top', '0').next()
                            .css('padding-top', '60px');

                } else {

                    // when scroll up or less than aboveHeight,
                    //  remove the “fixed” class, and the padding-top
                    $('nav').removeClass('fixed').next()
                            .css('padding-top', '0');
                }
            });
        });
    </script>
@stop
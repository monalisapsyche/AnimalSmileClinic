@extends('layout')
@section('content')


        <!-- #region Jssor Slider Begin -->


<style>
    .jssora106 {
        display: block;
        position: absolute;
        cursor: pointer;
    }

    .jssora106 .c {
        fill: #fff;
        opacity: .3;
    }

    .jssora106 .a {
        fill: none;
        stroke: #000;
        stroke-width: 350;
        stroke-miterlimit: 10;
    }

    .jssora106:hover .c {
        opacity: .5;
    }

    .jssora106:hover .a {
        opacity: .8;
    }

    .jssora106.jssora106dn .c {
        opacity: .2;
    }

    .jssora106.jssora106dn .a {
        opacity: 1;
    }

    .jssora106.jssora106ds {
        opacity: .3;
        pointer-events: none;
    }

    .jssort051 .p {
        position: absolute;
        top: 0;
        left: 0;
        background-color: #000;
    }

    .jssort051 .t {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        border: none;
        opacity: .45;
    }

    .jssort051 .p:hover .t {
        opacity: .8;
    }

    .jssort051 .pav .t, .jssort051 .pdn .t, .jssort051 .p:hover.pdn .t {
        opacity: 1;
    }
</style>

<div class="row">
    <div class="col-lg-12">
        <div id="jssor_1" class="img-responsive"
             style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:480px;overflow:hidden;visibility:hidden;">
            <!-- Loading Screen -->
            <div data-u="loading"
                 style="position:absolute;top:0px;left:0px;background:url('img/loading.gif') no-repeat 50% 50%;background-color:rgba(0, 0, 0, 0.7);"></div>
            <div data-u="slides"
                 style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:370px;overflow:hidden;">
                <div>
                    <img data-u="image" src="{{URL::to('img\bg\cat001.jpg')}}"/>
                    <img data-u="thumb" src="{{URL::to('img\bg\cat001.jpg')}}"/>
                </div>
                <div>
                    <img data-u="image" src="{{URL::to('img\bg\dog002.jpg')}}"/>
                    <img data-u="thumb" src="{{URL::to('img\bg\dog002.jpg')}}"/>
                </div>
                <div>
                    <img data-u="image" src="{{URL::to('img\bg\dog003.jpg')}}"/>
                    <img data-u="thumb" src="{{URL::to('img\bg\dog003.jpg')}}"/>
                </div>
                <div>
                    <img data-u="image" src="{{URL::to('img\bg\rat005.jpg')}}"/>
                    <img data-u="thumb" src="{{URL::to('img\bg\rat005.jpg')}}"/>
                </div>
                <div>
                    <img data-u="image" src="{{URL::to('img\bg\cat004.jpg')}}"/>
                    <img data-u="thumb" src="{{URL::to('img\bg\cat004.jpg')}}"/>
                </div>


                <a data-u="any" href="https://www.jssor.com" style="display:none">bootstrap slider</a>
            </div>
            <!-- Thumbnail Navigator -->
            <div data-u="thumbnavigator" class="jssort051"
                 style="position:absolute;left:0px;bottom:0px;width:980px;height:140px;" data-autocenter="1"
                 data-scale-bottom="0.75">
                <div data-u="slides">
                    <div data-u="prototype" class="p" style="width:200px;height:80px;">
                        <div data-u="thumbnailtemplate" class="t"></div>
                    </div>
                </div>
            </div>
            <!-- Arrow Navigator -->
            <div data-u="arrowleft" class="jssora106" style="width:60px;height:55px;top:162px;left:30px;"
                 data-scale="0.75">
                <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                    <polyline class="a" points="7930.4,5495.7 5426.1,8000 7930.4,10504.3 "></polyline>
                    <line class="a" x1="10573.9" y1="8000" x2="5426.1" y2="8000"></line>
                </svg>
            </div>
            <div data-u="arrowright" class="jssora106" style="width:55px;height:55px;top:162px;right:30px;"
                 data-scale="0.75">
                <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                    <polyline class="a" points="8069.6,5495.7 10573.9,8000 8069.6,10504.3 "></polyline>
                    <line class="a" x1="5426.1" y1="8000" x2="10573.9" y2="8000"></line>
                </svg>
            </div>
        </div>
    </div>
    <!----------------------------------------------banner----------------------------------------------------------------->

        <div class="w3-panel w3-light-gray w3-bottombar w3-border-gray w3-border">
            <br><br>
            <div align="center">
            <header class="w3-container w3-light-gray">

                    <h1>คลินิกสัตว์ยิ้มแย้ม</h1>
                    <p style="font-size: 170%"> ตรวจวินิจฉัยและรักษาโรคสัตว์ทางด้านอายุรกรรม ศัลยกรรม <br>และคลินิกเฉพาะทาง
                        เปิดให้บริการทุกวันเวลา 24 ชม. <br> เพื่อความสุขของคุณและสัตว์เลี้ยง</p>

            </header>
        </div>
            <br><br><br><br><br><br>
        </div>
</div>
<br>
<div style="margin-left:5%;margin-right:5%">
    <div class="row">
        <div class="col-md-12">
            <br><br><br>
            <div class="col-md-3">
                <a href="{{url('dognews')}}"><img src="{{URL::to('img\picture\but01.png')}}"
                                                  class="w3-hover-grayscale img-responsive"
                                                  style="max-width: 100%"></a>
            </div>
            <div class="col-md-3">
                <a href="{{url('catnews')}}"><img src="{{URL::to('img\picture\but02.png')}}"
                                                  class="w3-hover-grayscale img-responsive"
                                                  style="max-width: 100%"></a>
            </div>
            <div class="col-md-3">
                <a href="{{url('rabbitnews')}}"><img src="{{URL::to('img\picture\but03.png')}}"
                                                     class="w3-hover-grayscale img-responsive"
                                                     style="max-width: 100%"></a>
            </div>
            <div class="col-md-3">
                <a href="{{url('hamsternews')}}"><img src="{{URL::to('img\picture\but04.png')}}"
                                                      class="w3-hover-grayscale img-responsive"
                                                      style="max-width: 100%"></a>
            </div>
        </div>
    </div>
</div>
</div>

<br><br><br>

@stop

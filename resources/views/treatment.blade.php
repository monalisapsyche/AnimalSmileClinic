@extends('layout')
@section('content')

    <div class="row">
        <div class="row">
            <div class="col-xs-12">
                <div style="margin-left:7%;margin-right:0%">
                    <h2 class="hover-effect" style="color:#0fadff"><b> ขั้นตอนการเข้ารักษาสัตว์ป่วย </b></h2>
                    </span>
                </div>
            </div>
        </div>


        <div class="w3-panel w3-light-grey w3-leftbar w3-border-grey" style="margin-left:10%;margin-right:10%">
            <p>
            <h3>การทำบัตร</h3></p>
            <p>บริการทำบัตร
                วันจันทร์ถึงศุกร์
                08.00-17.00 น.
                ยกเว้นวันหยุดราชการและวันหยุดนักขัตฤกษ์
                เปิดทำการ 10.00-17.00 น. </p>
            <p>
            <h3>ทำบัตรใหม่</h3></p>
            <p>1. ติดต่อเจ้าหน้าที่เวชระเบียน</p>
            <p>2. ชั่งน้ำหนักสัตว์ป่วยทุกครั้งที่มารับการรักษา</p>
            <p>3. กรุณาพาสัตว์ป่วยไปนั่งรอตรวจโรคตามคำแนะนำ</p>
            <p>4. รอพบสัตวแพทย์ตามลำดับคิว</p>
            <p>5. รับใบนัดหมาย และใบสั่งยา พร้อมยื่นชำระค่าใช้จ่าย</p>
            <br>

            <form method="post" action="{{ url('/insertContact') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="w3-container">
                    <h2>พบหมอ</h2>
                    <h3>
                        <p>กรุณาใส่รายละเอียด :</p></h3>
                    <p>ชื่อ<input class="w3-input  w3-hover-gray" type="text" name="con_n" required></p>
                    <p>นามสกุล<input class="w3-input  w3-hover-gray" type="text" name="con_l" required></p>
                    <p>เบอร์โทร<input class="w3-input  w3-hover-gray" type="text" name="con_tel" required></p>
                    <p>อีเมลล์<input class="w3-input  w3-hover-gray" type="text" name="con_email" required></p>
                    <p>ที่อยู่<textarea class="w3-input  w3-hover-gray" name="con_address" rows="5" required></textarea></p>
                    <p>รายละเอียดการขอนัด<textarea class="w3-input  w3-hover-gray" name="con_detail"
                                                   rows="5" required></textarea></p>
                    <input type="hidden" name="con_date" value="{{date("Y-m-d")}}">
                    <p>
                        <button class="w3-button w3-blue w3-round-xlarge">ส่งข้อมูล</button>
                    </p>
                </div>
            </form>
        </div>
    </div>
    <br><br><br>
    <script>
        /*------------------------navbar------------------------------------------------*/
        $(document).ready(function () {

            //Calculate the height of <header>
            //Use outerHeight() instead of height() if have padding
            var aboveHeight = $('header').outerHeight();

            //when scroll
            $(window).scroll(function () {

                //if scrolled down more than the header’s height
                if ($(window).scrollTop() > aboveHeight) {

                    // if yes, add “fixed” class to the <nav>
                    // add padding top to the #content
                    //  (value is same as the height of the nav)
                    $('nav').addClass('fixed').css('top', '0').next()
                            .css('padding-top', '60px');

                } else {

                    // when scroll up or less than aboveHeight,
                    //  remove the “fixed” class, and the padding-top
                    $('nav').removeClass('fixed').next()
                            .css('padding-top', '0');
                }
            });
        });
    </script>
@stop

@extends('layout')
@section('content')

    <br><br>
    <div class="row">
        <div class="row">
            <div class="col-xs-12" color="#0000FF">
                <div style="margin-left:7%;margin-right:0%">
                    <h2 class="w3-text-blue" style="text-shadow:1px 1px 0 #444">
                        <span class="w3-tag w3-jumbo w3-purple">H</span>
                        <span class="w3-tag w3-jumbo w3-black">O</span>
                        <span class="w3-tag w3-jumbo w3-purple">T</span>
                        <span class="w3-tag w3-jumbo w3-black">E</span>
                        <span class="w3-tag w3-jumbo w3-purple">L</span>
                        <br><br>
                        <p class="w3-text-purple"><b>โรงแรมเพื่อสัตว์เลี้ยงที่คุณรัก</b></p></h2>
                    </span>
                </div>
            </div>
        </div>
        <br> <br>
        <div style="margin-left: 7%;margin-right: 7%">
            <div class="row">

                <div class="col-md-3">
                    <div class="w3-card-4">
                        <img src="{{URL::to('img\hotel\556151.jpg')}}" class="img-responsive" style="max-width: 110%">
                    </div>
                </div>
                <div class="col-md-3">
                    <br>
                    <ul>
                        <p class="w3-text-purple" style="font-size: 20px;"><b>﻿﻿﻿China Room</b></p>
                        <p><b>฿500.00</b></p>
                        <li>
                            <p style="font-size: 18px;">
                                ห้องขนาดใหญ่ 4.0*1.8 เมตร
                            </p>
                        </li>
                        <li>
                            <p style="font-size: 18px;">
                                น้องแมวพักได้ 1-6 ตัว
                            </p>
                        </li>
                        <li>
                            <p style="font-size: 18px;">
                                ชอบชมวิว
                            </p>
                        </li>
                        <li>
                            <p style="font-size: 18px;">
                                ชอบผาดโผน
                            </p>
                        </li>

                    </ul>
                </div>
                <div class="col-md-3">
                    <div class="w3-card-4">
                        <img src="{{URL::to('img\hotel\654546.jpg')}}"
                             class="img-responsive" style="max-width:110%">
                    </div>
                </div>
                <div class="col-md-3">
                    <br>
                    <ul>
                        <p class="w3-text-purple" style="font-size: 20px;">﻿﻿﻿<b>Japan Room</b></p>
                        <p><b>฿500.00</b></p>
                        <li>
                            <p style="font-size: 18px;">ห้องขนาดใหญ่ 3.0*2.1 เมตร</p>
                        </li>
                        <li>
                            <p style="font-size: 18px;">น้องแมวพักได้ 1-6 ตัว</p>
                        </li>
                        <li>
                            <p style="font-size: 18px;">ชอบความเป็นส่วนตัว</p>
                        </li>
                        <li>
                            <p style="font-size: 18px;">ชอบชมวิว</p>
                        </li>
                    </ul>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-3">
                    <div class="w3-card-4">
                        <img src="{{URL::to('img\hotel\565.jpg')}}"
                             class="img-responsive" style="max-width: 110%">
                    </div>
                </div>
                <div class="col-md-3">

                    <br>
                    <ul>

                        <p class="w3-text-purple" style="font-size: 20px;"><b>England Room</b></p>
                        <p><b>฿500.00</b></p>
                        <li>
                            <p style="font-size: 18px;">ห้องขนาดกลาง 1.6*1.8 เมตร</p>
                        </li>
                        <li>
                            <p style="font-size: 18px;">น้องแมวพักได้ 1-4 ตัว</p>
                        </li>
                        <li>
                            <p style="font-size: 18px;">ชอบความเป็นส่วนตัว</p>
                        </li>
                        <li>
                            <p style="font-size: 18px;">ชอบผาดโผน</p>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <div class="w3-card-4">
                        <img src="{{URL::to('img\hotel\66.jpg')}}" class="img-responsive" style="max-width: 110%">
                    </div>
                </div>
                <div class="col-md-3">
                    <br>
                    <ul>
                        <p class="w3-text-purple" style="font-size: 20px;">﻿﻿﻿<b>France Room</b></p>
                        <p><b>฿400.00</b></p>
                        <li>
                            <p style="font-size: 18px;">ห้องขนาดกลาง 1.6*1.8 เมตร</p>
                        </li>
                        <li>
                            <p style="font-size: 18px;">น้องแมวพักได้ 1-4 ตัว</p>
                        </li>
                        <li>
                            <p style="font-size: 18px;">ชอบชมวิว</p>
                        </li>
                        <li>
                            <p style="font-size: 18px;">ชอบความเป็นส่วนตัว</p>
                        </li>
                    </ul>
                </div>
            </div>
            <hr>
            <div class="col-md-3">
                <div class="w3-card-4">
                    <img src="{{URL::to('img\hotel\44.jpg')}}" class="img-responsive" style="max-width: 110%">
                </div>
            </div>
            <div class="col-md-3">
                <br>
                <ul>
                    <p class="w3-text-purple" style="font-size: 20px;">﻿﻿﻿<b>Greece Room</b></p>
                    <p><b>฿400.00</b></p>
                    <li>
                        <p style="font-size: 18px;">ห้องขนาดกลาง 1.6*2.1 เมตร</p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">น้องแมวพักได้ 1-4 ตัว</p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">ชอบผาดโผน</p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">ชอบความเป็นส่วนตัว</p>
                    </li>
                </ul>
            </div>


            <div class="col-md-3">
                <div class="w3-card-4">
                    <img src="{{URL::to('img\hotel\878.jpg')}}" class="img-responsive" style="max-width: 110%">
                </div>
            </div>
            <div class="col-md-3">
                <br>
                <ul>
                    <p class="w3-text-purple" style="font-size: 20px;">﻿﻿﻿<b>Finland Room</b></p>
                    <p><b>฿500.00</b></p>
                    <li>
                        <p style="font-size: 18px;">ห้องขนาดกลาง 1.6*2.1 เมตร</p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">น้องแมวพักได้ 1-4 ตัว</p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">ชอบชมวิว</p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">ชอบผาดโผน</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <br><br><br><br>

    <script>
        /*------------------------navbar------------------------------------------------*/
        $(document).ready(function () {

            //Calculate the height of <header>
            //Use outerHeight() instead of height() if have padding
            var aboveHeight = $('header').outerHeight();

            //when scroll
            $(window).scroll(function () {

                //if scrolled down more than the header’s height
                if ($(window).scrollTop() > aboveHeight) {

                    // if yes, add “fixed” class to the <nav>
                    // add padding top to the #content
                    //  (value is same as the height of the nav)
                    $('nav').addClass('fixed').css('top', '0').next()
                            .css('padding-top', '60px');

                } else {

                    // when scroll up or less than aboveHeight,
                    //  remove the “fixed” class, and the padding-top
                    $('nav').removeClass('fixed').next()
                            .css('padding-top', '0');
                }
            });
        });
    </script>
@stop
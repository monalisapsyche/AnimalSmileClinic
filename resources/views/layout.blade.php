<html>
<head>
    <!--------------------------------------i3school CSS -------------------------------------------------------------->
    <link rel="stylesheet" href="{{ URL::asset('https://www.w3schools.com/w3css/4/w3.css') }}">
    <!--------------------------------------bootstrap CSS ------------------------------------------------------------->
    <link rel="stylesheet" href="{{ URL::asset('bootstrap/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('bootstrap/css/bootstrap.min.css') }}">
    <!--------------------------------------- Glyphicons -------------------------------------------------------------->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!--------------------------------------- i3school Modal ---------------------------------------------------------->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript"
            src="{{asset('https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('js/slider/js/jquery-1.11.3.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/slider/js/jssor.slider-25.0.7.min.js')}}"></script>
    <!-- slider -->
    <script type="text/javascript" src="{{asset('slider01/js/jssor.slider-25.1.0.min')}}"></script>
    <!----------------------------------------------------------------------------------------------------------------->
    <!-- Datepicker -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="{{asset('https://code.jquery.com/jquery-1.12.4.js')}}"></script>
    <script src="{{asset('https://code.jquery.com/ui/1.12.1/jquery-ui.js')}}"></script>
    <!----------------------------------------------------------------------------------------------------------------->
</head>
<body
        style="padding:0px; margin:0px; background-color:#fff;font-family:'Open Sans',sans-serif,arial,helvetica,verdana">
<!--
<div class="w3-panel w3-grey">
    <div style="margin-right:5%">
        <div align="right">
             <img src="{{URL::to('img\L\b9_4.jpg')}}" width="31" height="21">
            <img src="{{URL::to('img\L\british-flag-clip-art-5082.jpg')}}" width="33" height="19">
        </div>

    </div>
</div>
-->

<style>
    #wrapper {
        width: 940px;
        margin: 0 auto;
    }

    header {
        text-align: center;
        padding: 70px 0;
    }

    nav {
        height: 150px;
        width: 1360px;

        position: relative;
    }

    #content {
        background: #fff;
        height: 1500px; /* presetting the height */
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.3);
    }

    .fixed {
        position: fixed;
    }
</style>

<nav class="w3-gray navbar-static-top bg-faded">
    <div style="margin-left:5%;margin-right:10%">
        <br>
        <a href="{{url('/')}}"><img src="{{URL::to('img\logo\mymylogo.png')}}" class="img-responsive" style="max-width: 20%"></a>
    </div>

    <div align="right">
        <div style="margin-right:5%">
            <a href="{{url('/')}}" class="w3-button skyblue">
                <h4>
                    <div class="fontthai"> หน้าแรก</div>
                </h4>
            </a>

            <a href="{{url('treatment')}}" class="w3-button skyblue"><h4> พบหมอ </h4></a>
            <div class="w3-dropdown-hover">
                <button class="w3-button skyblue"><h4>การบริการ</h4></button>
                <div class="w3-dropdown-content w3-bar-block w3-card-4">
                    <a href="{{url('product')}}" class="w3-bar-item w3-button">สินค้า</a>
                    <a href="{{url('spa')}}" class="w3-bar-item w3-button">สปา</a>
                    <a href="{{url('shower')}}" class="w3-bar-item w3-button">อาบน้ำ / ตัดขน</a>
                </div>
            </div>
            <div class="w3-dropdown-hover">
                <button class="w3-button skyblue"><h4>ฝากเลี้ยง</h4></button>
                <div class="w3-dropdown-content w3-bar-block w3-card-4">
                    <a href="{{url('hoteldog')}}" class="w3-bar-item w3-button">สุนัข</a>
                    <a href="{{url('hotelcat')}}" class="w3-bar-item w3-button">แมว</a>
                    <a href="{{url('hotelother')}}" class="w3-bar-item w3-button">อื่นๆ</a>
                </div>
            </div>
            <a href="{{url('information')}}" class="w3-button skyblue"><h4> ข่าวสารน่ารู้ </h4></a>
            <a href="{{url('Contactus')}}" class="w3-button skyblue"><h4> ติดต่อเรา </h4></a>
        </div>
    </div>
</nav>


@yield('content')


<div class="row" align="right">
    <div style="margin-right: 7%">
        <div class="col-md-3"></div>
        <div class="col-md-9">
            <a href="{{url('https://www.facebook.com/molovebucky')}}"> <img
                        src="{{URL::to('img\logo\facebook.ico')}}" style="max-width: 10%"></a>
            &nbsp;&nbsp;
            <a href="{{url('https://www.instagram.com/monalisa_psyche/')}}"> <img
                        src="{{URL::to('img\logo\instagram.png')}}" style="max-width: 10%"></a>
            &nbsp;&nbsp;
            <a href="{{url('https://twitter.com/monalisa_psyche')}}"> <img
                        src="{{URL::to('img\logo\twitter.ico')}}" style="max-width: 10%"></a>
        </div>

    </div>
</div>

<div class="w3-panel w3-gray">
    <h4 class="w3-opacity" align="center">100/161 ซอย 3 หมู่ 8 หมู่บ้านลานทอง ตำบล บางพูด อำเภอ ปากเกร็ด จังหวัด
        นนทบุรี 1120
        เบอร์โทรศัพท์ 02-583-2057 , 02-962-4465 <br>E-mail : Molovebucky_barnes@hotmail.com ,
        Killualovepatty@hotmail.com</h4>
</div>


<script type="text/javascript">
    jssor_1_slider_init = function () {

        var jssor_1_SlideshowTransitions = [
            {
                $Duration: 1200,
                x: 0.3,
                $During: {$Left: [0.3, 0.7]},
                $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                x: -0.3,
                $SlideOut: true,
                $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                x: -0.3,
                $During: {$Left: [0.3, 0.7]},
                $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                x: 0.3,
                $SlideOut: true,
                $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                y: 0.3,
                $During: {$Top: [0.3, 0.7]},
                $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                y: -0.3,
                $SlideOut: true,
                $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                y: -0.3,
                $During: {$Top: [0.3, 0.7]},
                $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                y: 0.3,
                $SlideOut: true,
                $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                x: 0.3,
                $Cols: 2,
                $During: {$Left: [0.3, 0.7]},
                $ChessMode: {$Column: 3},
                $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                x: 0.3,
                $Cols: 2,
                $SlideOut: true,
                $ChessMode: {$Column: 3},
                $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                y: 0.3,
                $Rows: 2,
                $During: {$Top: [0.3, 0.7]},
                $ChessMode: {$Row: 12},
                $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                y: 0.3,
                $Rows: 2,
                $SlideOut: true,
                $ChessMode: {$Row: 12},
                $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                y: 0.3,

                $Cols: 2,
                $During: {$Top: [0.3, 0.7]},
                $ChessMode: {$Column: 12},
                $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                y: -0.3,
                $Cols: 2,
                $SlideOut: true,
                $ChessMode: {$Column: 12},
                $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                x: 0.3,
                $Rows: 2,
                $During: {$Left: [0.3, 0.7]},
                $ChessMode: {$Row: 3},
                $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                x: -0.3,
                $Rows: 2,
                $SlideOut: true,
                $ChessMode: {$Row: 3},
                $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2

            },
            {
                $Duration: 1200,
                x: 0.3,
                y: 0.3,
                $Cols: 2,
                $Rows: 2,
                $During: {$Left: [0.3, 0.7], $Top: [0.3, 0.7]},
                $ChessMode: {$Column: 3, $Row: 12},
                $Easing: {$Left: $Jease$.$InCubic, $Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                x: 0.3,
                y: 0.3,
                $Cols: 2,
                $Rows: 2,
                $During: {$Left: [0.3, 0.7], $Top: [0.3, 0.7]},
                $SlideOut: true,
                $ChessMode: {$Column: 3, $Row: 12},
                $Easing: {$Left: $Jease$.$InCubic, $Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                $Delay: 20,
                $Clip: 3,
                $Assembly: 260,
                $Easing: {$Clip: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },

            {
                $Duration: 1200,
                $Delay: 20,
                $Clip: 3,
                $SlideOut: true,
                $Assembly: 260,
                $Easing: {$Clip: $Jease$.$OutCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                $Delay: 20,
                $Clip: 12,
                $Assembly: 260,
                $Easing: {$Clip: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            },
            {
                $Duration: 1200,
                $Delay: 20,
                $Clip: 12,
                $SlideOut: true,
                $Assembly: 260,
                $Easing: {$Clip: $Jease$.$OutCubic, $Opacity: $Jease$.$Linear},
                $Opacity: 2
            }
        ];

        var jssor_1_options = {
            $AutoPlay: 1,
            $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,

                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
            },
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
            },
            $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $Cols: 5,
                $Align: 400
            }
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

        /*#region responsive code begin*/
        /*remove responsive code if you don't want the slider scales while window resizing*/
        function ScaleSlider() {
            var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
            if (refSize) {
                refSize = Math.min(refSize, 1350);
                jssor_1_slider.$ScaleWidth(refSize);
            }
            else {
                window.setTimeout(ScaleSlider, 30);
            }
        }

        ScaleSlider();
        $Jssor$.$AddEvent(window, "load", ScaleSlider);
        $Jssor$.$AddEvent(window, "resize", ScaleSlider);
        $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);

        /*#endregion responsive code end*/
    };


    jssor_1_slider_init();

    /*------------------------navbar------------------------------------------------*/
    $(document).ready(function () {

        //Calculate the height of <header>
        //Use outerHeight() instead of height() if have padding
        var aboveHeight = $('header').outerHeight();

        //when scroll
        $(window).scroll(function () {

            //if scrolled down more than the header’s height
            if ($(window).scrollTop() > aboveHeight) {

                // if yes, add “fixed” class to the <nav>
                // add padding top to the #content
                //  (value is same as the height of the nav)
                $('nav').addClass('fixed').css('top', '0').next()
                        .css('padding-top', '60px');

            } else {

                // when scroll up or less than aboveHeight,
                //  remove the “fixed” class, and the padding-top
                $('nav').removeClass('fixed').next()
                        .css('padding-top', '0');
            }
        });
    });
</script>


</body>
</html>







































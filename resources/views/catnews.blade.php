@extends('layout')
@section('content')

    <div class="col-xs-12">
        <style>
            ol {
                background: #ff9999;
                padding: 20px;
            }

            ol li {
                background: #ffe5e5;
                padding: 5px;
                margin-left: 35px;
            }
        </style>

        <div style="margin-left:7%;margin-right:0%">
            <h2 class="hover-effect" style="color:#DC143C"><b> วิธีการ สังเกตอาการป่วยของแมว </b></h2>
        </div>
        <div style="margin-left:9%;margin-right:0%">
            <h3 class="hover-effect" style="color: #ff9999"><b> 3 วิธีการ: </b></h3>
        </div>
        <div style="margin-left:7%;margin-right:9%">
            <ol>
                <li> สังเกตลักษณะท่าทางและพฤติกรรมที่เปลี่ยนไป</li>
                <li> สังเกตอาการผิดปกติ</li>
                <li> สังเกตอาการเฉพาะโรค</li>
                <br>
                หนึ่งในเหตุผลที่คนชอบเลี้ยงแมวก็คือท่าทางผ่อนคลายสบายใจซะเหลือเกินนี่แหละ แมวนั้นเกิดมาเพื่อทำอะไรตามใจ
                ชีวิตชิลล์ซะจนคนยังอิจฉา วันๆ เอาแต่กิน นอน แล้วก็เล่น
                แต่รู้ไหมนิสัยที่ว่าอาจเป็นดาบสองคมเอาตอนแมวป่วยนี่แหละ เพราะแมวจะหนีหายหลบหน้าไปเลยตามสัญชาตญาณ
                ไม่ก็ทำกิจวัตรประจำวันที่ว่า (อย่างการนอน) เยอะเกินจนน่าเป็นห่วง จะรู้ได้ว่าแมวคุณป่วยขึ้นมาจริงๆ
                หรือเปล่า คุณต้องหัดสังเกตอาการและพฤติกรรมต่างๆ ที่เปลี่ยนไป
            </ol>
        </div>
    </div>
    <div style="margin-left:9%;margin-right:0%">
        <h3 class="hover-effect" style="color: #ff9999"><b> 1.สังเกตลักษณะท่าทางและพฤติกรรมที่เปลี่ยนไป : </b></h3>
    </div>
    <div class="row" style="margin-left:25%;margin-right:0%">
        <img src="{{URL::to('img\Picture\1.jpg')}}" style="max-width: 65%">
    </div>
    <br>
    <style>
        ul {
            background: #ff9999;
            padding: 20px;
        }

        ul li {
            background: #ffe5e5;
            padding: 10px;
            margin-left: 35px;
        }
    </style>
    <div style="margin-left:7%;margin-right:9%">
        <ul>
            <li> &nbsp;1.1 : แมวนอนมากแค่ไหน. ถ้าแมวป่วยจะนอนเยอะเป็นพิเศษ ถ้าแมวไม่มีอาการผิดปกติอื่นๆ อย่างอ้วก
                ท้องเสีย
                ไม่กินอาหาร หรือตัวบวม ให้คุณคอยสังเกตการณ์ต่อไป ถ้ามีอาการที่ว่าเมื่อไหร่ ให้พาไปหาหมอทันที
                ถ้าแมวไม่มีอาการอื่นร่วมด้วย ให้จับตาดู 24 ชั่วโมง (หรือจะพาไปหาหมอก่อนก็ได้ถ้าคุณเป็นห่วงมาก)
                ถ้าวันที่ 2 แล้วแมวยังนอนเยอะดูเพลีย ให้รีบพาไปหาหมอเลย
            </li>
        </ul>
    </div>
    <div class="row" style="margin-left:25%;margin-right:0%">
        <img src="{{URL::to('img\Picture\2.jpg')}}" style="max-width: 65%">
    </div>
    <br>
    <style>
        ul {
            background: #ff9999;
            padding: 20px;
        }

        ul li {
            background: #ffe5e5;
            padding: 10px;
            margin-left: 35px;
        }
    </style>
    <div style="margin-left:7%;margin-right:9%">
        <ul>
            <li> &nbsp;1.2 : วัดไข้หน่อย. ให้ใช้เทอร์โมมิเตอร์แบบวัดไข้ในรูก้น
                แต่ถ้าแมวไม่ยอมหรือกลัวทำแมวเครียดกว่าเดิมก็ค่อยให้คุณหมอวัดไข้ก็ได้
                อุณหภูมิ 37 - 39 องศาถือว่าปกติดี แต่ถ้าเกิน 39 ขึ้นไปแสดงว่าเริ่มตัวร้อนแล้ว ยิ่งเกิน 39 นิดๆ
                ขึ้นไปนี่ก็ไข้แล้วล่ะ
                รีบพาไปหาหมอเลย
            </li>
            <br>
            แมวเป็นไข้มักนอนเยอะผิดปกติ ไม่ยอมกินอาหาร และขนด้านชี้โด่ชี้เด่
            จมูกกับหูก็อาจแห้งและอุ่นได้เวลาเอานิ้วแตะตอนอุณหภูมิตัวปกติ
            ถึงปกติการจับหูแมวเพื่อวัดไข้นั้นเป็นวิธีที่ไม่ค่อยได้ผล แต่ถ้าจับแล้วหูแมวเย็นก็แสดงว่าไม่น่าเป็นไข้หรอก
        </ul>
    </div>
    <div class="row" style="margin-left:25%;margin-right:0%">
        <img src="{{URL::to('img\Picture\3.jpg')}}" style="max-width: 65%">
    </div>
    <br>
    <style>
        ul {
            background: #ff9999;
            padding: 20px;
        }

        ul li {
            background: #ffe5e5;
            padding: 10px;
            margin-left: 35px;
        }
    </style>
    <div style="margin-left:7%;margin-right:9%">
        <ul>
            <li> &nbsp;1.3 : พฤติกรรมการขับถ่ายของแมวเปลี่ยนไปหรือเปล่า. ลองสังเกตดูว่าแมวอึฉี่บ่อยแค่ไหน อึฉี่ลำบากไหม
                มีเลือดหรือมูกอะไรปนมาในฉี่หรือเปล่า
                และอึเป็นก้อนแข็งผิดปกติไหม[3]ถ้าตอนแรกแมวท้องเสียแล้วต่อมากลับไม่ยอมอึหรือท้องผูก
                (อึเป็นก้อนแข็ง) ให้รีบพาไปหาหมอด่วน รวมถึงถ้าแมวไม่อึไม่ฉี่หรืออึฉี่แต่มีเลือดปนด้วย
            </li>
            <br>
            แมวตัวผู้มักมีปัญหาเรื่องทางเดินปัสสาวะบ่อยกว่า โดยเฉพาะเรื่องฉี่ไม่ออก
            สัญญาณบอกเหตุคือแมวจะแวะเวียนไปที่กระบะทรายบ่อยขึ้น
            หรือกระทั่งมานั่งยองๆ นอกกระบะ แมวอาจนั่งเบ่งอยู่อย่างนั้นนานเป็นหลายนาทีหรือลุกเดินไปนั่งยองๆ
            ที่จุดอื่นเรื่อยๆ
            ถ้าแมวมีอาการแบบนี้ให้สังเกตว่าแมวฉี่ออกหรือเปล่า (พื้นแห้งหรือเปียก?) ถ้าแมวฉี่ ก็ดูด้วยว่ามีเลือดปนไหม
        </ul>
    </div>
    <div class="row" style="margin-left:25%;margin-right:0%">
        <img src="{{URL::to('img\Picture\4.jpg')}}" style="max-width: 65%">
    </div>
    <br>
    <style>
        ul {
            background: #ff9999;
            padding: 20px;
        }

        ul li {
            background: #ffe5e5;
            padding: 10px;
            margin-left: 35px;
        }
    </style>
    <div style="margin-left:7%;margin-right:9%">
        <ul>
            <li> &nbsp;1.4 : สังเกตเรื่องอาหารการกิน. ถ้าคุณสังเกตเห็นว่าแมวไม่ค่อยกินหรือกลับกันคือกินเยอะกว่าปกติ
                ก็คือผิดปกติทั้งคู่
                ถ้าแมวไม่กินอาหารทั้งวัน เป็นไปได้หลายอย่าง เช่น แอบไปกินอาหารบ้านข้างๆ มา รู้สึกคลื่นไส้
                หรืออาจมีปัญหาเรื่องไต แต่ถ้าอยู่ๆ
                แมวเกิดตะกละกินเยอะเป็นพิเศษ อาจแปลว่ามีปัญหาสุขภาพบางอย่าง
            </li>
            <br>
            ถ้าแมวไม่ยอมกินอาหารนานเกิน 24 ชั่วโมง ให้พาไปตรวจร่างกายกับคุณหมอ
            จะได้รักษาให้ตรงจุดก่อนเกิดภาวะหรือโรคแทรกซ้อน
        </ul>
    </div>
    <div style="margin-left:9%;margin-right:0%">
        <h3 class="hover-effect" style="color: #ff9999"><b> 2.สังเกตอาการผิดปกติ : </b></h3>
    </div>
    <div class="row" style="margin-left:25%;margin-right:0%">
        <img src="{{URL::to('img\Picture\11.jpg')}}" style="max-width: 65%">
    </div>
    <br>
    <style>
        ul {
            background: #ff9999;
            padding: 20px;
        }

        ul li {
            background: #ffe5e5;
            padding: 10px;
            margin-left: 35px;
        }
    </style>
    <div style="margin-left:7%;margin-right:9%">
        <ul>
            <li> &nbsp;2.1 : แมวอ้วกไหม. ถ้าแมวอ้วกหรืออ้วกเยอะวันละหลายครั้ง แถมดูเหนื่อยๆ ป่วยๆ
                แปลว่าน่าจะมีอะไรผิดปกติ
                ถ้าแมวไม่ยอมกินน้ำหรืออ้วกหลังกินน้ำ ควรพาไปหาหมอแล้วล่ะ
            </li>
            <br>
        </ul>
    </div>
    <div class="row" style="margin-left:25%;margin-right:0%">
        <img src="{{URL::to('img\Picture\6.jpg')}}" style="max-width: 65%">
    </div>
    <br>
    <style>
        ul {
            background: #ff9999;
            padding: 20px;
        }
        ul li {
            background: #ffe5e5;
            padding: 10px;
            margin-left: 35px;
        }
    </style>
    <div style="margin-left:7%;margin-right:9%">
        <ul>
            <li> &nbsp;2.2 :
                แมวร่าเริงดีไหม. ถ้าดูเซื่องซึม ไม่ค่อยมีแรง แสดงว่าอาจมีไข้ หายใจลำบาก หรือเจ็บปวดตรงไหน จะต่างกับอีกอาการที่แมวนอนเยอะเกิน
                ตรงที่แบบนี้แมวตื่นแต่ไม่ค่อยมีแรงหรือดูไม่อยากทำอะไรที่เคยทำในแต่ละวัน ถ้าแมวดูซึมแถมหายใจหอบถี่ ก็พาไปหาหมอดีกว่า
            </li><br>
        </ul>
    </div>
    <div class="row" style="margin-left:25%;margin-right:0%">
        <img src="{{URL::to('img\Picture\7.jpg')}}" style="max-width: 65%">
    </div>
    <br>
    <style>
        ul {
            background: #ff9999;
            padding: 20px;
        }
        ul li {
            background: #ffe5e5;
            padding: 10px;
            margin-left: 35px;
        }
    </style>
    <div style="margin-left:7%;margin-right:9%">
        <ul>
            <li> &nbsp;2.3 :
                แมวคอเอียง ดูงงๆ มึนๆ หรือวิงเวียนไหม. เหล่านี้เป็นอาการของโรคเกี่ยวกับระบบประสาท ไม่ก็อาการติดเชื้อในหู
                ถ้าแมวแสดงอาการพวกนี้ให้รีบพาไปหาหมอ ปกติแมวมักปราดเปรียว ลุกยืนคล่องแคล่วว่องไว ถ้าอยู่ๆ แมวเกิดซุ่มซ่ามหรือคอเอียงแปลกๆ
                แสดงว่ามีอะไรผิดปกติ อาจเป็นสัญญาณบอกโรคหลอดเลือดในสมอง ความดันสูง หรือกระทั่งเนื้องอกในสมอง
                ทางที่ดีให้เอาไปตรวจร่างกายกันไว้ก่อน
            </li><br>
        </ul>
    </div>
    <div style="margin-left:9%;margin-right:0%">
        <h3 class="hover-effect" style="color: #ff9999"><b> 3.สังเกตอาการเฉพาะโรค : </b></h3>
    </div>
    <div class="row" style="margin-left:25%;margin-right:0%">
        <img src="{{URL::to('img\Picture\8.jpg')}}" style="max-width: 65%">
    </div>
    <br>
    <style>
        ul {
            background: #ff9999;
            padding: 20px;
        }
        ul li {
            background: #ffe5e5;
            padding: 10px;
            margin-left: 35px;
        }
    </style>
    <div style="margin-left:7%;margin-right:9%">
        <ul>
            <li> &nbsp;3.1 :
                แมวมีหมัดหรือเปล่า. ถ้าเกาซะอุ้งเท้าเป็นระวิง แปลว่าน่าจะมี ถ้าคุณเห็นแมวเกาแกรกๆ ประจำ ก็ให้อุ้มมาสำรวจขนกันแบบละเอียดหน่อย
                หาหวีเสนียดหรือหวีซี่ถี่มา แล้วใช้สางขนแมวหาหมัด ถ้ามีจะหน้าตาเหมือนจุดสีน้ำตาลเล็กๆ วิ่งเร็วๆ โดยเฉพาะแถวคอกับหาง
            </li><br>
        </ul>
    </div>
    <div class="row" style="margin-left:25%;margin-right:0%">
        <img src="{{URL::to('img\Picture\9.jpg')}}" style="max-width: 65%">
    </div>
    <br>
    <style>
        ul {
            background: #ff9999;
            padding: 20px;
        }
        ul li {
            background: #ffe5e5;
            padding: 10px;
            margin-left: 35px;
        }
    </style>
    <div style="margin-left:7%;margin-right:9%">
        <ul>
            <li> &nbsp;3.2 :
                ถ้าแมวไอแห้งและอ้วก อาจเป็นเพราะก้อนขนอุดตัน. Hairballs หรือก้อนขน (สะสมจากที่แมวเลียตัว)
                ทำให้แมวปากเหม็นหรือไม่กินอาหารได้ ยิ่งถ้าก้อนขนในกระเพาะเยอะๆ เข้าอาจกลายเป็น Trichobezoars
                (ก้อนขนจับตัวแข็งกับอาหารที่ไม่ย่อยส่งกลิ่นเหม็น) ซึ่งถือว่าร้ายแรงมากจนต้องผ่าตัดลูกเดียว เพราะงั้นคุณต้องหมั่นดูแลขนน้องแมวบ่อยๆ
                จะได้ลดก้อนขน
            </li><br>
        </ul>
    </div>
    <div class="row" style="margin-left:25%;margin-right:0%">
        <img src="{{URL::to('img\Picture\10.jpg')}}" style="max-width: 65%">
    </div>
    <br>
    <style>
        ul {
            background: #ff9999;
            padding: 20px;
        }
        ul li {
            background: #ffe5e5;
            padding: 10px;
            margin-left: 35px;
        }
    </style>
    <div style="margin-left:7%;margin-right:9%">
        <ul>
            <li> &nbsp;3.3 :
                สังเกตอาการโรคทางเดินปัสสาวะส่วนล่างในแมว (feline lower urinary tract disease หรือ FLUTD). สัญญาณบอกโรค FLUTD
                ก็เช่น ฉี่ยาก ฉี่น้อย หรือฉี่บ่อย ไม่กินอาหาร เฉื่อยชา ฉี่มีเลือดปน หรือก้มเลียตรงนั้นของตัวเองบ่อยๆ
                โรคนี้เป็นอาการทางเดินปัสสาวะส่วนล่างอักเสบที่ทำให้เจ็บปวดรุนแรง และลุกลามได้ง่าย
            </li><br>
        </ul>
    </div>
@stop
@extends('layout')
@section('content')

    <br><br>
    <div class="row">
        <div class="row">
            <div class="col-xs-12" color="#0000FF">
                <div style="margin-left:7%;margin-right:0%">
                    <h2 class="w3-text-blue" style="text-shadow:1px 1px 0 #444">
                        <span class="w3-tag w3-jumbo w3-purple">H</span>
                        <span class="w3-tag w3-jumbo w3-black">O</span>
                        <span class="w3-tag w3-jumbo w3-purple">T</span>
                        <span class="w3-tag w3-jumbo w3-black">E</span>
                        <span class="w3-tag w3-jumbo w3-purple">L</span>
                        <br><br>
                        <p class="w3-text-purple"><b>โรงแรมเพื่อสัตว์เลี้ยงที่คุณรัก</b></p></h2>
                    </span>
                </div>
            </div>
        </div>
        <br> <br>
        <div style="margin-left: 7%;margin-right: 7%">
            <div class="row">

                <div class="col-md-3">
                    <div class="w3-card-4">
                        <img src="{{URL::to('img\hotel\g1.jpg')}}" class="img-responsive" style="max-width: 100%">
                    </div>
                </div>
                <div class="col-md-3">
                    <br>
                    <ul>
                        <p class="w3-text-purple" style="font-size: 20px;"><b>﻿﻿﻿กระรอก</b></p>
                        <p><b>฿100.00</b></p>
                        <li>
                            <p style="font-size: 18px;">
                                อยู่กรงส่วนตัว มีของใช้ในกรงครบ
                            </p>
                        </li>
                        <li>
                            <p style="font-size: 18px;">
                                สามารถนำน้องกระรอกมาอยู่กรงเดียวกันได้<br>
                                (ในกรณีฝากมากกว่า 1 ตัวราคาคิดเป็นต่อตัว)
                            </p>
                        </li>
                        <li>
                            <p style="font-size: 18px;">
                                มีอาหารให้วันละ 2 มื้อ<br>
                                (ไม่ร่วมขนมกับของว่างอีก 2 มื้อ)
                            </p>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <div class="w3-card-4">
                        <img src="{{URL::to('img\hotel\g2.jpg')}}"
                             class="img-responsive" style="max-width:100%">
                    </div>
                </div>
                <div class="col-md-3">
                    <br>
                    <ul>
                        <p class="w3-text-purple" style="font-size: 20px;">﻿﻿﻿<b>กระรอก</b></p>
                        <p><b>฿70.00</b></p>
                        <li>
                            <p style="font-size: 18px;">อยู่กรงส่วนตัว มีของใช้ในกรงครบ</p>
                        </li>
                        <li>
                            <p style="font-size: 18px;">สามารถนำน้องกระรอกมาอยู่กรงเดียวกันได้<br>
                                (ในกรณีฝากมากกว่า 1 ตัวราคาคิดเป็นต่อตัว)</p>
                        </li>
                        <li>
                            <p style="font-size: 18px;">ไม่รวมค่าอาหาร<br>
                                (กรณีนำอาหารมาเอง)</p>
                        </li>

                    </ul>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-3">
                    <div class="w3-card-4">
                        <img src="{{URL::to('img\hotel\s1.jpg')}}"
                             class="img-responsive" style="max-width: 100%">
                    </div>
                </div>
                <div class="col-md-3">
                    <br>
                    <ul>
                        <p class="w3-text-purple" style="font-size: 20px;"><b>ชูก้า</b></p>
                        <p><b>฿70.00</b></p>
                        <li>
                            <p style="font-size: 18px;">อยู่กรงส่วนตัว มีของใช้ในกรงครบ</p>
                        </li>
                        <li>
                            <p style="font-size: 18px;">สามารถนำน้องชูก้ามาอยู่กรงเดียวกันได้<br>
                                (ในกรณีฝากมากกว่า 1 ตัวราคาคิดเป็นต่อตัว)</p>
                        </li>
                        <li>
                            <p style="font-size: 18px;"> มีอาหารให้วันละ 2 มื้อ<br>
                                (ไม่ร่วมขนมกับของว่างอีก 2 มื้อ)</p>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <div class="w3-card-4">
                        <img src="{{URL::to('img\hotel\s2.jpg')}}" class="img-responsive" style="max-width: 100%">
                    </div>
                </div>
                <div class="col-md-3">
                    <br>
                    <ul>
                        <p class="w3-text-purple" style="font-size: 20px;">﻿﻿﻿<b>ชูก้า</b></p>
                        <p><b>฿50.00</b></p>
                        <li>
                            <p style="font-size: 18px;">อยู่กรงส่วนตัว มีของใช้ในกรงครบ</p>
                        </li>
                        <li>
                            <p style="font-size: 18px;">สามารถนำน้องชูก้ามาอยู่กรงเดียวกันได้<br>
                                (ในกรณีฝากมากกว่า 1 ตัวราคาคิดเป็นต่อตัว)</p>
                        </li>
                        <li>
                            <p style="font-size: 18px;">ไม่รวมค่าอาหาร<br>
                                (กรณีนำอาหารมาเอง)</p>
                        </li>
                    </ul>
                </div>
            </div>
            <hr>
            <div class="col-md-3">
                <div class="w3-card-4">
                    <img src="{{URL::to('img\hotel\r1.jpg')}}" class="img-responsive" style="max-width: 100%">
                </div>
            </div>
            <div class="col-md-3">
                <br>
                <ul>
                    <p class="w3-text-purple" style="font-size: 20px;">﻿﻿﻿<b>กระต่าย</b></p>
                    <p><b>฿100.00</b></p>
                    <li>
                        <p style="font-size: 18px;">อยู่กรงส่วนตัว มีของใช้ในกรงครบ</p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">สามารถนำน้องกระต่ายมาอยู่กรงเดียวกันได้<br>
                            (ในกรณีฝากมากกว่า 1 ตัวราคาคิดเป็นต่อตัว)</p>
                    </li>
                    <li>
                        <p style="font-size: 18px;"> มีอาหารให้วันละ 2 มื้อ<br>
                            (ไม่ร่วมขนมกับของว่างอีก 2 มื้อ)</p>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="w3-card-4">
                    <img src="{{URL::to('img\hotel\r2.jpg')}}" class="img-responsive" style="max-width: 110%">
                </div>
            </div>
            <div class="col-md-3">
                <br>
                <ul>
                    <p class="w3-text-purple" style="font-size: 20px;">﻿﻿﻿<b>สัตว์เลี้ยงชนิดอื่นๆ</b></p>
                    <p><b>฿70.00 - ฿100.00</b></p>
                    <li>
                        <p style="font-size: 18px;">มีกรงหลายขนาด จัดตามความเหมาะสม</p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">มีของใช้จำเป็นในกรงให้ครบ<br>
                            (ของใช้ในกรงขึ้นอยู่กับสัตว์เลี้ยงที่นำมาฝาก)</p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">มีอาหารให้วันละ 2 มื้อ<br>
                            (ไม่ร่วมขนมกับของว่างอีก 2 มื้อ)</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <br><br><br><br>

    <script>
        /*------------------------navbar------------------------------------------------*/
        $(document).ready(function () {

            //Calculate the height of <header>
            //Use outerHeight() instead of height() if have padding
            var aboveHeight = $('header').outerHeight();

            //when scroll
            $(window).scroll(function () {

                //if scrolled down more than the header’s height
                if ($(window).scrollTop() > aboveHeight) {

                    // if yes, add “fixed” class to the <nav>
                    // add padding top to the #content
                    //  (value is same as the height of the nav)
                    $('nav').addClass('fixed').css('top', '0').next()
                            .css('padding-top', '60px');

                } else {

                    // when scroll up or less than aboveHeight,
                    //  remove the “fixed” class, and the padding-top
                    $('nav').removeClass('fixed').next()
                            .css('padding-top', '0');
                }
            });
        });
    </script>
@stop
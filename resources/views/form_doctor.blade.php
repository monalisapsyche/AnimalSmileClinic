@extends('layout')
@section('content')
    <div style="margin-left: 15%;margin-right: 15%;">
        <div align="center">
            เวลานัด (สำหรับสัตวแพทย์)
        </div>


        <br>
        ชื่อสัตวแพทย์
        <select class="form-control">
            <option value="00">หมอ</option>
        </select>
        <br>
        รหัสสัตว์เลี้ยง <input class="form-control" type="text" name="ani_no">
        <br>
        วัน-เวลา ที่นัด <input class="form-control" name="sch_datetime" value="ปฏิทิน">
        <br>
        หมายเหตุ
        <textarea class="form-control" name="ani_remark"></textarea>
        <br><br>
        <div align="center">
            <input class="btn btn-info" name="sub" type="submit" value="SAVE">
        </div>
    </div>
@stop
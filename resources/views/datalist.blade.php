@extends('layout')
@section('content')

    <nav class="navbar-static-top" style="background-color: steelblue">
        <div style="margin-left:5%;margin-right:5%">
            <br>
            <div class="row" align="right">
                <div class="col-lg-12">

                    <a href="{{url('datalist')}}" class="w3-button skyblue"><h4> HOME </h4></a>
                    <a href="{{url('form_customer')}}" class="w3-button skyblue"><h4> ยังไม่มีประวัติ </h4></a>
                    <a href="{{url('datalist_animal')}}" class="w3-button skyblue"><h4> จัดการสัตว์เลี้ยง </h4></a>
                    <a href="index.php" class="w3-button skyblue"><h4> ตารางนัดสัตวแพทย์ </h4></a>
                    <div class="w3-dropdown-hover skyblue">
                        <button class="w3-button"><h4>อัพเดต</h4></button>
                        <div class="w3-dropdown-content w3-bar-block w3-border">
                            <a href="#" class="w3-bar-item w3-button">สินค้า</a>
                            <a href="#" class="w3-bar-item w3-button">บริการ</a>
                            <a href="#" class="w3-bar-item w3-button">ข่าวสาร</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <br><br>

    <div class="form-inline" align="center">
        <input class="form-control" type="text" name="serCus">
    </div>
    <br>
    <div align="center">
        ลูกค้าที่มีประวัติแล้ว
    </div>
    <br>
    <div class="col-md-12">
        <div style="margin-left: 15%;margin-right: 15%">
            <table class="table table-condensed table-responsive">
                <tr>
                    <td class="bg-info">ชื่อ-นามสกุล</td>
                    <td class="bg-info">เบอร์โทร</td>
                    <td class="bg-info">อัพเดตข้อมูล</td>
                    <td class="bg-info">เพิ่มสัตว์เลี้ยง</td>
                </tr>
                @foreach($selCus as $s)
                    <tr>
                        <td>{{$s->cus_name}}</td>
                        <td>{{$s->cus_phone}}</td>
                        <td><a href="{{url('view_editcustomer')}}?viewCustomer={{ $s->cus_ID }}" class="btn btn-info">VIEW/ EDIT</a></td>
                        <td><a href="{{url('form_animal')}}?addAnimal={{ $s->cus_ID }}" class="btn btn-info"><span
                                        class="glyphicon glyphicon-plus"></span> </a></td>
                    </tr>
                @endforeach
            </table>
        </div>

        <br><br>


    </div>
    <br><br>














@stop
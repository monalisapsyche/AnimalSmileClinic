@extends('layout')
@section('content')

    <style>
        /* jssor slider loading skin double-tail-spin css */

        .jssorl-004-double-tail-spin img {
            animation-name: jssorl-004-double-tail-spin;
            animation-duration: 1.2s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-004-double-tail-spin {
            from {
                transform: rotate(0deg);
            }

            to {
                transform: rotate(360deg);
            }
        }

        .jssorb052 .i {
            position: absolute;
            cursor: pointer;
        }

        .jssorb052 .i .b {
            fill: #000;
            fill-opacity: 0.3;
            stroke: #fff;
            stroke-width: 400;
            stroke-miterlimit: 10;
            stroke-opacity: 0.7;
        }

        .jssorb052 .i:hover .b {
            fill-opacity: .7;
        }

        .jssorb052 .iav .b {
            fill-opacity: 1;
        }

        .jssorb052 .i.idn {
            opacity: .3;
        }

        .jssora053 {
            display: block;
            position: absolute;
            cursor: pointer;
        }

        .jssora053 .a {
            fill: none;
            stroke: #fff;
            stroke-width: 640;
            stroke-miterlimit: 10;
        }

        .jssora053:hover {
            opacity: .8;
        }

        .jssora053.jssora053dn {
            opacity: .5;
        }

        .jssora053.jssora053ds {
            opacity: .3;
            pointer-events: none;
        }
    </style>

    <div class="row">
        <div style="margin-left:7%;margin-right:7%">
            <div class="row">
                <div class="col-xs-12">
                    <div style="margin-left:7%;">
                        <br>
                        <h2 class="w3-text-blue" style="text-shadow:1px 1px 0 #444">
                            <b>จำหน่ายสินค้าภายในคลินิก</b></h2>


                    </div>
                </div>
            </div>
            <br><br>
            <div id="jssor_1"
                 style="position:relative;margin:0 auto;top:0px;left:0px;width:930px;height:380px;overflow:hidden;visibility:hidden;">
                <!-- Loading Screen -->
                <div data-u="loading" class="jssorl-004-double-tail-spin"
                     style="position:absolute;top:0px;left:0px;text-align:center;background-color:rgba(0,0,0,0.7);">
                    <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;"
                         src="{{url('slider01/img/double-tail-spin.svg')}}"/>
                </div>
                <div data-u="slides"
                     style="cursor:default;position:relative;top:0px;left:0px;width:930px;height:380px;overflow:hidden;">
                    <div>
                        <img data-u="image" src="{{URL::to('img\product\02.jpg')}}" style="max-width: 100%"/>
                    </div>
                    <div>
                        <img data-u="image" src="{{URL::to('img\product\03.jpg')}}" style="max-width: 100%"/>
                    </div>
                    <div>
                        <img data-u="image" src="{{URL::to('img\product\04.jpg')}}" style="max-width: 100%"/>
                    </div>
                    <div>
                        <img data-u="image" src="{{URL::to('img\product\05.jpg')}}" style="max-width: 100%"/>
                    </div>
                    <div>
                        <img data-u="image" src="{{URL::to('img\product\08.jpg')}}" style="max-width: 100%"/>
                    </div>
                    <div>
                        <img data-u="image" src="{{URL::to('img\product\09.jpg')}}" style="max-width: 100%"/>
                    </div>

                    <a data-u="any" href="https://www.jssor.com" style="display:none">html carousel</a>
                </div>
                <!-- Bullet Navigator -->
                <div data-u="navigator" class="jssorb052" style="position:absolute;bottom:12px;right:12px;"
                     data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
                    <div data-u="prototype" class="i" style="width:16px;height:16px;">
                        <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                            <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                        </svg>
                    </div>
                </div>
                <!-- Arrow Navigator -->
                <div data-u="arrowleft" class="jssora053" style="width:55px;height:55px;top:0px;left:25px;"
                     data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                    <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                        <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
                    </svg>
                </div>
                <div data-u="arrowright" class="jssora053" style="width:55px;height:55px;top:0px;right:25px;"
                     data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                    <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                        <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
                    </svg>
                </div>
            </div>
            <script type="text/javascript">jssor_1_slider_init();</script>
            <br>

            <div class="row">
               <h3><b> บริการตัดแต่งขนเสริมสวยให้น้องหมา และการอาบน้ำด้วย CO2 เป็นหนึ่งในนวัตกรรมใหม่ที่ช่วยดูแลขน<br>
                       และบำรุงสุขภาพผิวหนังสัตว์เลี้ยง</b></h3><br>
              <h4>  &nbsp; &nbsp; <span class="glyphicon glyphicon-star"></span> คลินิกสัตว์ยิ้มแย้ม เปิดบริการตั้งแต่ 8.00 - 22.00 มีสินค้าและบริการเกี่ยวกับสัตว์เลี้ยงมากมาย <br><br>
                &nbsp; &nbsp; <span class="glyphicon glyphicon-star"></span> ไม่ว่าจะเป็นอาหารยี่ห้อต่างๆ ที่นอนและเสื้อผ้าสัตว์เลี้ยงที่สามารถหาซื้อได้ตามงานเทศกาล
                ขนมน้องหมาน้องแมวพร้อมทั้งสัตว์เลี้ยงพิเศษ <br>  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;หรือแม้แต่เค้กของสัตว์เลี้ยงที่เจ้าของสามารถจัดให้น้องในวันพิเศษ ได้เลย <br><br>
                &nbsp; &nbsp; <span class="glyphicon glyphicon-star"></span> สามารถโทรสอบถาม และสั่งซื้อสินค้าได้ที่เบอร์ <b>02-583-2057</b></h4>
            </div>
        </div>
    </div>
    <br><br><br>


    <script type="text/javascript">
        jssor_1_slider_init = function () {

            var jssor_1_options = {
                $AutoPlay: 1,
                $Idle: 2000,
                $SlideEasing: $Jease$.$InOutSine,
                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$
                },
                $BulletNavigatorOptions: {
                    $Class: $JssorBulletNavigator$
                }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/
            /*remove responsive code if you don't want the slider scales while window resizing*/
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 980);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();
            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>

@stop
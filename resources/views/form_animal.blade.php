@extends('layout')
@section('content')
    <div style="margin-left: 15%;margin-right: 15%;">
        <div align="center">
            ประวัติสัตว์เลี้ยง (ยังไม่มีประวัติ)
        </div>
        <br><br>
        <form action="{{url('/insertAnimal')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            @foreach($add as $add)
                <div class="form-inline">
                     <input class="form-control" type="hidden" name="cus_no" value="{{$add->cus_no}}" readonly>
                    ชื่อลูกค้า <input class="form-control" type="text" value="{{$add->cus_name}}" readonly>
                </div>
            @endforeach
            <br>
            ชื่อสัตว์เลี้ยง <input class="form-control" type="type" name="ani_name">
            <br>
            ประเภท
            <select class="form-control" name="typeAnimal">
                <option value="D">สุนัข</option>
                <option value="C">แมว</option>
                <option value="O">อื่นๆ</option>
            </select>
            <br>
            ระบุ
            <input class="form-control" name="ani_outher" placeholder="ประเภทสัตว์ อื่นๆ">
            <br>
            สายพันธุ์ <input class="form-control" type="type" name="ani_breed">
            <br>
            อาการป่วย <textarea class="form-control" type="type" name="ani_sick"></textarea>
            <br>
            หมายเหตุ <textarea class="form-control" name="ani_remark"></textarea>
            <br>
            วันที่บันทึก
            <div class="form-inline">
                <input class="form-control" type="text" name="ani_createdate" value="{{date("Y-m-d")}}" readonly>
            </div>

            <br><br>
            <input type="hidden" name="ani_createdate2" value="{{date("Y-m-d")}}">

            <div align="center">
                <input class="btn btn-info" type="submit" value="SAVE">
            </div>
        </form>
    </div>
@stop
@extends("layout")
@section('content')
    <div style="margin-top: 5%">
        <div class="col-md-3"></div>
        <div class="col-md-6">

            <div align="center">
                <h4><b>ประวัติลูกค้า</b></h4>
            </div>
            <br><br>



            @foreach($view as $view)
                <div class="form-inline">
                    ชื่อลูกค้า <input class="form-control" type="text" value="{{$view->cus_name}}" name="cus_name"
                                      size="50">

                    <br><br>
                    ที่อยู่ <input class="form-control" type="text" value="{{$view->cus_address}}" size="71"
                                   name="cus_address">
                    <br><br>
                    <div class="form-inline">

                        จังหวัด : <select class="form-control" name="province_ID" id="provinceid"
                                          onchange="getArea(this.value)">
                            <option value=""> -- Select --</option>
                            @foreach($pro as $pro)
                                <option value="{{$pro->PROVINCE_ID}}">{{$pro->PROVINCE_NAME}}</option>
                            @endforeach
                        </select>&nbsp;&nbsp;
                        อำเภอ : <select class="form-control" name="amphoe_ID" id="areaid"
                                        onchange="getDestination(this.value)">
                            <option value=""> -- Select --</option>

                        </select>
                        <br><br>

                        ตำบล : <select class="form-control" name="district_ID" id="desid"
                                       onchange="getZipcode(this.value)">
                            <option value=""> -- Select --</option>
                        </select>

                        รหัสไปรษณีย์ <select class="form-control" name="zipcode_ID" id="zipid">
                            <option value=""> -- Select --</option>
                        </select>
                    </div>
                    <br>
                    เบอร์โทรศัพท์ <input class="form-control" type="text" value="{{$view->cus_phone}}" size="64"
                                         name="cus_phone">
                    <br><br>
                    E-mail <input class="form-control" type="text" value="{{$view->cus_mail}}" size="70"
                                  name="cus_mail">
                    <br><br>
                    หมายเหตุ <br>
                    <textarea class="form-control" name="cus_remark" cols="78" rows="5">{{$view->cus_remark}}</textarea>
                    <br><br>
                    วันที่อัพเดตล่าสุด <input class="form-control" type="text" value="{{$view->create_date}}"
                                              readonly="">

                    <input class="form-control" type="hidden" value="{{$view->create_date}}" name="create_date"
                           size="70">
                    <input class="form-control" type="hidden" value="{{$view->create_date2}}" name="create_date2"
                           size="70">
                </div>
            @endforeach


        </div>

        <div class="col-md-3"></div>
    </div>


    </div>













@stop


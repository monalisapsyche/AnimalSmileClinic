@extends('layout')
@section('content')


    <div class="form-inline" align="center">
        <input class="form-control" type="text" name="serCus">
    </div>
    <br>
    <div align="center">
        สัตว์เลี้ยงที่มีประวัติแล้ว
    </div>
    <br>
    <div class="col-md-12">
        <div style="margin-left: 15%;margin-right: 15%">
            <table class="table table-condensed table-responsive">
                <tr>
                    <td class="bg-info">รหัส</td>
                    <td class="bg-info">ชื่อสัตว์</td>
                    <td class="bg-info">ประวัติการรักษา</td>
                    <td class="bg-info">เพิ่มการรักษา</td>
                </tr>
                @foreach($selAni as $s)
                    <tr>
                        <td>{{$s->ani_no}}</td>
                        <td>{{$s->ani_name}}</td>
                        <td><a href="{{url('view_editcustomer')}}?viewCustomer={{ $s->ani_ID }}" class="btn btn-info">
                                Treatment</a></td>
                        <td><a href="{{url('form_animal')}}?addAnimal={{ $s->ani_ID }}" class="btn btn-info"><span
                                        class="glyphicon glyphicon-plus"></span> </a></td>
                    </tr>
                @endforeach
            </table>
        </div>

        <br><br>


    </div>
    <br><br>

@stop

@extends('layout')
@section('content')

    <div style="margin-left: 15%;margin-right: 15%;">
        <div align="center">
            ประวัติเจ้าของสัตว์เลี้ยง (ยังไม่มีประวัติ)
        </div>
        <br><br>
        <form action="{{url('insertCostomer')}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">


            ชื่อ-นามสกุล <input class="form-control" type="text" name="cus_name"><br>
            ที่อยู่ <input class="form-control" type="text" name="cus_address">
            <br>
            <div class="form-inline">

                จังหวัด : <select class="form-control" name="province_ID" id="provinceid"
                                  onchange="getArea(this.value)">
                    <option value=""> -- Select --</option>
                    @foreach($pro as $pro)
                        <option value="{{$pro->PROVINCE_ID}}">{{$pro->PROVINCE_NAME}}</option>
                    @endforeach
                </select>&nbsp;&nbsp;
                อำเภอ : <select class="form-control" name="amphoe_ID" id="areaid"
                                onchange="getDestination(this.value)">
                    <option value=""> -- Select --</option>

                </select>
                &nbsp;&nbsp;
                ตำบล : <select class="form-control" name="district_ID" id="desid"
                               onchange="getZipcode(this.value)">
                    <option value=""> -- Select --</option>
                </select>

                รหัสไปรษณีย์ <select class="form-control" name="zipcode_ID" id="zipid">
                    <option value=""> -- Select --</option>
                </select>
            </div>
            <br>
            เบอร์โทร <input class="form-control" type="text" name="cus_phone">
            <br>
            E-mail <input class="form-control" type="text" name="cus_mail">
            <br>
            หมายเหตุ <textarea class="form-control" type="text" name="cus_remark"></textarea>
            <br>
            วันที่บันทึก
            <div class="form-inline">
                <input class="form-control" type="text" name="create_date" value="{{date("Y-m-d")}}" readonly>
                <input class="form-control" type="hidden" name="create_date2" value="{{date("Y-m-d")}}" readonly>
            </div>

            <br><br>
            <div align="center">
                <input class="btn btn-info" type="submit" name="subCustomer" value="SAVE">
            </div>
        </form>
    </div>



    <script>

        function getArea(id) {
            var province_id = id;
            $.ajax({
                type: 'GET',
                url: 'ajaxsend/' + province_id,
                success: function (data) {
                    //   alert(data);
                    $('#areaid').html(data);
                }
            });
        }


        function getDestination(id) {
            var area_id = id;
            $.ajax({
                type: 'GET',
                url: 'ajaxsend/' + area_id,
                success: function (data) {
                    //   alert(data);
                    $('#desid').html(data);
                }
            });
        }

        function getZipcode(id) {
            var des_id = id;
            $.ajax({
                type: 'GET',
                url: 'ajaxsend/' + des_id,
                success: function (data) {
                    //   alert(data);
                    $('#desid').html(data);
                }
            });
        }

    </script>















@stop
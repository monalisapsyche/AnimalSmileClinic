@extends('layout')
@section('content')

    <br><br><br>
    <div class="row">
        <div class="col-xs-12" color="#0000FF">
            <div style="margin-left:7%;margin-right:0%">
                <h2 class="w3-text-blue" style="text-shadow:1px 1px 0 #444">
                    <span class="w3-tag w3-jumbo w3-purple">S</span>
                    <span class="w3-tag w3-jumbo w3-black">P</span>
                    <span class="w3-tag w3-jumbo w3-purple">A</span>
                    <br>
                    <p class="w3-text-purple"><b>บริการสปาโดยการนวดผ่อนคลาย</b></p></h2>
                </span>
            </div>
        </div>
    </div>
    <br> <br>
    <div style="margin-left: 7%;margin-right: 7%">
        <div class="row">

            <div class="col-md-3">
                <div class="w3-card-4">
                    <img src="{{URL::to('img\Picture\0_45.jpg')}}" class="img-responsive" style="max-width: 300%">
                </div>
            </div>
            <div class="col-md-3">
                <br>
                <ul>
                    <p class="w3-text-purple" style="font-size: 20px;"><b>﻿﻿﻿สปาการนวดผ่อนคลาย</b></p>
                 <li>
                        <p style="font-size: 18px;">
                            ทำให้ผ่อนคลายกล้ามเนื้อจากการใช้ร่างกายในรูปแบบเดิมๆ เป็นเวลานาน
                        </p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">
                            ช่วยผ่อนคลายความเครียดจากการถูกขังอยู่ในกรงหรือถูกทิ้งไว้ตัวเดียว
                        </p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">
                            ช่วยให้สภาพจิตใจผ่อนคลาย
                        </p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">
                            ช่วยให้ระบบต่างๆ ในร่างกายหมุนเวียนได้ดีขึ้น
                        </p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">
                            นวดด้วยการสัมผัส
                            เหมาะสำหรับสัตว์เลี้ยงทุกสายพันธุ๋
                            ใช้เวลาประมาณ 45 นาที
                        </p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">
                            ราคา : 299 บาท
                        </p>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="w3-card-4">
                    <img src="{{URL::to('img\Picture\55_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1.jpg')}}"
                         class="img-responsive" style="max-width:300%">
                </div>
            </div>
            <div class="col-md-3">
                <br>
                <ul>
                    <p class="w3-text-purple" style="font-size: 20px;">﻿﻿﻿<b>เกลือทะเลสปา</b></p>

                    <li>
                        <p style="font-size: 18px;">บำรุงรักษาผิวหนังที่มีปัญหา</p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">ทำให้คราบฝังแน่นตามเส้นขนและผิวหนังหลุดออก</p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">ทำให้สุนัขผ่อนคลายจากกลิ่นของเกลือสปา</p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">ทำให้สัตว์เลี้ยงรู้สึกสดชื่นกระปรี้กระเปร้า</p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">หมักด้วยเกลือทะเลสปาเหมาะสำหรับ
                            สัตว์เลี้ยงที่เครียดไม่มีเวลาออกกำลังกายหรือมีปัญหาเรื่องผิวหนัง
                            ใช้เวลาประมาณ 40 นาที</p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">ราคา : 399 บาท</p>
                    </li>
                </ul>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-3">
                <div class="w3-card-4">
                    <img src="{{URL::to('img\Picture\55_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1.jpg')}}"
                         class="img-responsive" style="max-width: 300%">
                </div>
            </div>
            <div class="col-md-3">

                <br>
                <ul>

                    <p class="w3-text-purple" style="font-size: 20px;"><b>﻿﻿﻿Hotoil</b></p>

                    <li>
                        <p style="font-size: 18px;">ทำให้ขนประกายเงางาม</p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">ทำให้ขนนุ่มมีน้ำหนัก</p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">ทำให้ผิวหนังชุ่มชื่น</p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">ลดการเกิดเส้นขนพันกัน</p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">ลดอาการผิวหนังแห้งเป็นขุย</p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">หมักด้วยเกลือทะเลสปาเหมาะสำหรับ สัตว์เลี้ยงสายพันธุ์ขนยาว
                            ใช้เวลาประมาณ 1 ชั่วโมง 30 นาที</p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">ราคา : 499 บาท</p>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="w3-card-4">
                    <img src="{{URL::to('img\Picture\66_1_1_1_1.jpg')}}" class="img-responsive" style="max-width: 100%">
                </div>
            </div>
            <div class="col-md-3">
                <br>
                <ul>
                    <p class="w3-text-purple" style="font-size: 20px;">﻿﻿﻿<b>Treatment</b></p>

                    <li>
                        <p style="font-size: 18px;">เพื่อลดการชี้ฟูทำให้ขนนุ่มลื่น</p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">ฟื้นฟูสภาพเส้นขนให้กลับมามีสุขภาพดี</p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">ป้องกันการหยิกเป็นฝอยและความชื้นจากภายนอก</p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">หมักด้วย Treatmentเหมาะสำหรับสัตว์เลี้ยงทุกสายพันธุ๋
                            ใช้เวลาประมาณ 1 ชั่วโมง 30 นาที</p>
                    </li>
                    <li>
                        <p style="font-size: 18px;">ราคา : 499 บาท</p>
                    </li>
                </ul>
            </div>
        </div>

    </div>
    <br><br><br><br>
    <script>
        /*------------------------navbar------------------------------------------------*/
        $(document).ready(function () {

            //Calculate the height of <header>
            //Use outerHeight() instead of height() if have padding
            var aboveHeight = $('header').outerHeight();

            //when scroll
            $(window).scroll(function () {

                //if scrolled down more than the header’s height
                if ($(window).scrollTop() > aboveHeight) {

                    // if yes, add “fixed” class to the <nav>
                    // add padding top to the #content
                    //  (value is same as the height of the nav)
                    $('nav').addClass('fixed').css('top', '0').next()
                            .css('padding-top', '60px');

                } else {

                    // when scroll up or less than aboveHeight,
                    //  remove the “fixed” class, and the padding-top
                    $('nav').removeClass('fixed').next()
                            .css('padding-top', '0');
                }
            });
        });
    </script>
@stop
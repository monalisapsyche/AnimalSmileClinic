@extends('layout')
@section('content')
    <br>

    <form method="post" action="{{ url('/insertShower') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div style="margin-left:10%;margin-right:10%">
            <h2><b>บริการอาบน้ำ / ตัดขน</b></h2>
            <br>
            <div class="form-inline">
                <div class="col-md-6">
                    <select class="w3-select  w3-border" name="type_ID" id="type_ID" required
                            onchange="getSer(this.value)">
                        <option value="" disabled selected>เลือกประเภทสัตว์เลี้ยง</option>
                        @foreach($selA as $selA)
                            <option value="{{$selA->type_ID}}">{{$selA->type_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6">
                    <select class="w3-select  w3-border" name="ser_ID" id="ser_ID" required
                            onchange="getShower(this.value)">
                        <option value="" disabled selected>บริการ</option>
                    </select>
                </div>
            </div>
            <br><br>
            <body onload="getShower('0')">
            <div class="dogShower">
                <h3><br>บริการอาบน้ำสำหรับสุนัข</h3>
                <div class="form-inline">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <td><b>ขนาดสุนัข</b></td>
                                <td><b>ราคา</b></td>
                            </tr>
                            <tr>
                                <td>ไซส์ S ( น้ำหนัก 1.5 กก. - 10 กก.)</td>
                                <td><input class="w3-check" type="radio" value="300" name="sho_price"
                                           onclick="PriceShower(this)"> 300 บาท
                                </td>
                            </tr>
                            <tr>
                                <td>ไซส์ M ( น้ำหนัก 11 กก. - 20 กก.)</td>

                                <td><input class="w3-check" type="radio" value="350" name="sho_price"
                                           onclick="PriceShower(this)"> 350 บาท
                                </td>
                            </tr>
                            <tr>
                                <td>ไซส์ L ( น้ำหนัก 20 กก. ขึ้นไป )</td>
                                <td><input class="w3-check" type="radio" value="400" name="sho_price"
                                           onclick="PriceShower(this)"> 400 บาท
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <h3>บริการเสริม
                    <input class="w3-check" type="radio" value="0" name="extra_price"
                           onclick="PriceOther(this)"> ไม่</h3>

                <div class="form-inline">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <td><b>บริการ</b></td>
                                <td><b>ราคา</b></td>
                            </tr>
                            <tr>
                                <td>กำจัดเห็บ หมัด ไร</td>
                                <td><input class="w3-check" type="radio" value="50" name="extra_price"
                                           onclick="PriceOther(this)"> 50 บาท
                                </td>
                            </tr>
                            <tr>
                                <td>กำจัดเห็บ หมัด ไร ( ตัดเล็บ )</td>
                                <td><input class="w3-check" type="radio" value="100" name="extra_price"
                                           onclick="PriceOther(this)"> 100 บาท
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>


            <div class="dogCutShower">
                <h3><br>บริการอาบน้ำ / ตัดขน สำหรับสุนัข</h3>
                <div class="form-inline">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <td><b>ขนาดสุนัข</b></td>
                                <td><b>ราคา</b></td>
                            </tr>
                            <tr>
                                <td>ไซส์ S ( น้ำหนัก 1.5 กก. - 10 กก.)</td>

                                <td><input class="w3-check" type="radio" name="sho_price" value="450"
                                           onclick="PriceShower(this)"> 450 บาท
                                </td>
                            </tr>
                            <tr>
                                <td>ไซส์ M ( น้ำหนัก 11 กก. - 20 กก.)</td>

                                <td><input class="w3-check" type="radio" name="sho_price" value="500"
                                           onclick="PriceShower(this)"> 500 บาท
                                </td>
                            </tr>
                            <tr>
                                <td>ไซส์ L ( น้ำหนัก 20 กก. ขึ้นไป )</td>

                                <td><input class="w3-check" type="radio" name="sho_price" value="550"
                                           onclick="PriceShower(this)"> 550 บาท
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <h3>บริการเสริม <input class="w3-check" type="radio" value="0" name="extra_price"
                                       onclick="PriceOther(this)"> ไม่</h3>
                <div class="form-inline">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <td><b>บริการ</b></td>
                                <td><b>ราคา</b></td>
                            </tr>
                            <tr>
                                <td>กำจัดเห็บ หมัด ไร</td>
                                <td><input class="w3-check" type="radio" name="extra_price" value="50"
                                           onclick="PriceOther(this)"> 50 บาท
                                </td>
                            </tr>
                            <tr>
                                <td>กำจัดเห็บ หมัด ไร ( ตัดเล็บ )</td>
                                <td><input class="w3-check" type="radio" name="extra_price" value="100"
                                           onclick="PriceOther(this)"> 100 บาท
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

            </div>

            <div class="catShower">
                <h3><br>บริการอาบน้ำสำหรับแมว</h3>
                <div class="form-inline">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <td><b>บริการ</b></td>
                                <td><b>ราคา</b></td>
                            </tr>
                            <tr>
                                <td>อาบน้ำ</td>
                                <td><input class="w3-check" type="radio" value="200" name="sho_price"
                                           onclick="PriceShower(this)"> 200 บาท
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <h3>บริการเสริม <input class="w3-check" type="radio" value="0" name="extra_price"
                                       onclick="PriceOther(this)"> ไม่</h3>
                <div class="form-inline">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <td><b>บริการ</b></td>
                                <td><b>ราคา</b></td>
                            </tr>
                            <tr>
                                <td>กำจัดเห็บ หมัด ไร</td>
                                <td><input class="w3-check" type="radio" value="50" name="extra_price"
                                           onclick="PriceOther(this)"> 50 บาท
                                </td>
                            </tr>
                            <tr>
                                <td>กำจัดเห็บ หมัด ไร ( ตัดเล็บ )</td>
                                <td><input class="w3-check" type="radio" value="100" name="extra_price"
                                           onclick="PriceOther(this)"> 100 บาท
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

            </div>

            <div class="catCutShower">
                <h3><br>บริการอาบน้ำ / ตัดขน สำหรับแมว</h3>
                <div class="form-inline">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <td><b>บริการ</b></td>
                                <td><b>ราคา</b></td>
                            </tr>
                            <tr>
                                <td>อาบน้ำ</td>
                                <td><input class="w3-check" type="radio" value="350" name="sho_price"
                                           onclick="PriceShower(this)"> 350 บาท
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <h3>บริการเสริม <input class="w3-check" type="radio" value="0" name="extra_price"
                                       onclick="PriceOther(this)"> ไม่</h3>
                <div class="form-inline">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <td><b>บริการ</b></td>
                                <td><b>ราคา</b></td>
                            </tr>
                            <tr>
                                <td>กำจัดเห็บ หมัด ไร</td>
                                <td><input class="w3-check" type="radio" value="50" name="extra_price"
                                           onclick="PriceOther(this)"> 50 บาท
                                </td>
                            </tr>
                            <tr>
                                <td>กำจัดเห็บ หมัด ไร ( ตัดเล็บ )</td>
                                <td><input class="w3-check" type="radio" value="100" name="extra_price"
                                           onclick="PriceOther(this)"> 100 บาท
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>


            <input type="hidden" id="num01">
            <input type="hidden" id="num02">
            </body>

            <br>
            <h3>กรุณากรอกรายละเอียด</h3>
            <p><br><br>ชื่อ<input class="w3-input  w3-hover-gray" type="text" name="cus_n"></p>
            <p>นามสกุล<input class="w3-input  w3-hover-gray" type="text" name="cus_n"></p>
            <p>เบอร์โทร<input class="w3-input  w3-hover-gray" type="text" name="cus_phone"></p>
            <p>อีเมลล์<input class="w3-input  w3-hover-gray" type="text" name="cus_mail"></p>
            <br>
            <input type="hidden" name="cus_date" value="{{date("Y-m-d")}}">
            <br>
            <div class="form-inline">
                ราคารวมทั้งหมด <input class="form-control" type="text" name="total" id="sum" readonly>
            </div>
            <br>
            <p>
                <button class="w3-button w3-blue w3-round-xlarge">ส่งข้อมูล</button>
            </p>

        </div>
        <br><br>
    </form>
    <script>
        function getSer(id) {
            var typeAnimal_id = id;
            $.ajax({
                type: 'GET',
                url: 'ajaxSendAnimal/' + typeAnimal_id,
                success: function (data) {
                    // alert(data);
                    $('#ser_ID').html(data);
                }
            });
        }

        function getShower(id) {

            var shoWer = id;
            if (shoWer == 1) {
                $(".dogShower").show();
            } else {
                $(".dogShower").hide();
            }

            if (shoWer == 2) {
                $(".dogCutShower").show();
            } else {
                $(".dogCutShower").hide();
            }

            if (shoWer == 3) {
                $(".catShower").show();
            } else {
                $(".catShower").hide();
            }

            if (shoWer == 4) {
                $(".catCutShower").show();
            } else {
                $(".catCutShower").hide();
            }

        }

        /*------------------------คำนวณค่าอาบน้ำ------------------------------------------------*/
        function PriceShower(number) {
            var num1 = number.value;
            $('#num01').val(num1);

        }
        function PriceOther(number) {
            var num2 = number.value;
            $('#num02').val(num2);
            sumPrice()
        }


        function sumPrice() {
            var num1 = document.getElementById("num01").value;
            var num2 = document.getElementById("num02").value;


            Ans = parseFloat(num1) + parseFloat(num2);
            // document.getElementById("DivAns").innerHTML = Ans;
            $('#sum').val(Ans); // ใช้แบบกัน input
        }

    </script>
    <script>
        /*------------------------navbar------------------------------------------------*/
        $(document).ready(function () {

            //Calculate the height of <header>
            //Use outerHeight() instead of height() if have padding
            var aboveHeight = $('header').outerHeight();

            //when scroll
            $(window).scroll(function () {

                //if scrolled down more than the header’s height
                if ($(window).scrollTop() > aboveHeight) {

                    // if yes, add “fixed” class to the <nav>
                    // add padding top to the #content
                    //  (value is same as the height of the nav)
                    $('nav').addClass('fixed').css('top', '0').next()
                            .css('padding-top', '60px');

                } else {

                    // when scroll up or less than aboveHeight,
                    //  remove the “fixed” class, and the padding-top
                    $('nav').removeClass('fixed').next()
                            .css('padding-top', '0');
                }
            });
        });


    </script>
@stop